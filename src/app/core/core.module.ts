import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthGuard } from './auth.guard';
import { AuthService } from './auth.service';
import { CustomInterceptorService } from './custom-interceptor.service';
import { TestService } from './test.service';

@NgModule({
  declarations: [],
  imports: [
    CommonModule
  ],
  providers: [
    CoreModule,
    AuthGuard,
    AuthService,
    CustomInterceptorService,
    TestService]
})
export class CoreModule {
  constructor() {
    console.log('core module init')
  }}
