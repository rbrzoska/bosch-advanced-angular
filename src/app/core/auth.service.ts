import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable()
export class AuthService {

  token = 'asdasdasdasdsfwergtrhh';
  constructor(){}
  private isLoggedIn$ = new BehaviorSubject<boolean>(true);

  get userStatus$(): Observable<boolean> {
    return this.isLoggedIn$.asObservable();
  }

  logIn() {
    this.isLoggedIn$.next(true);
  }

  logOut() {
    this.isLoggedIn$.next(false);
  }
}
