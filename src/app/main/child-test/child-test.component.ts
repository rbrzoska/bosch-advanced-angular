import { ChangeDetectionStrategy, ChangeDetectorRef, Component, Input, OnInit } from '@angular/core';
import { Product } from '../../models/product';

@Component({
  selector: 'bsh-child-test',
  templateUrl: './child-test.component.html',
  styleUrls: ['./child-test.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ChildTestComponent implements OnInit {

  counter: number = 0;
  @Input() data: Product;
  constructor(private cdr: ChangeDetectorRef) { }

  ngOnInit() {
    this.cdr.detach();
  }
  increment() {
    this.counter++;
  }

  runChangeDetector() {
    this.cdr.detectChanges();
  }

}
