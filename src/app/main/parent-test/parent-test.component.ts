import { Component, OnInit } from '@angular/core';
import { Product } from '../../models/product';

@Component({
  selector: 'bsh-parent-test',
  templateUrl: './parent-test.component.html',
  styleUrls: ['./parent-test.component.scss']
})
export class ParentTestComponent implements OnInit {

  parentData: Product = {
    title: 'test Title',
    description: 'description',
    price: 1.3
  };
  constructor() { }

  ngOnInit() {
  }

  changeParentData() {
    // this.parentData = {
    // ...this.parentData,
    //   title: 'new title'
    // }
    this.parentData.title = 'new title'
  }

}
