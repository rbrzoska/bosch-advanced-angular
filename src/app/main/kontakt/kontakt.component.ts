import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'bsh-kontakt',
  templateUrl: './kontakt.component.html',
  styleUrls: ['./kontakt.component.scss']
})
export class KontaktComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  sendMessage(form: NgForm) {
    console.log(form.value);
    form.reset();
  }
}
