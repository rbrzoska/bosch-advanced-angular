import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'bsh-home',
  template: `
    <p class="red" *ngIf="showTitle">
      home works!
    </p>
    
    <button class="btn btn-primary">Home button</button>
  `,
  styles: [`.red { color: red}`],
})
export class HomeComponent implements OnInit {

  showTitle = false;
  constructor() { }

  ngOnInit() {
  }

}
