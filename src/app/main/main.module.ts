import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MainRoutingModule } from './main-routing.module';
import { HomeComponent } from './home/home.component';
import { KontaktComponent } from './kontakt/kontakt.component';
import { NavigationComponent } from './navigation/navigation.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { SharedModule } from '../shared/shared.module';
import { FormsModule } from '@angular/forms';
import { ParentTestComponent } from './parent-test/parent-test.component';
import { ChildTestComponent } from './child-test/child-test.component';

@NgModule({
  declarations: [
    HomeComponent,
    KontaktComponent,
    NavigationComponent,
    PageNotFoundComponent,
    ParentTestComponent,
    ChildTestComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
    FormsModule,
    MainRoutingModule
  ],
  exports: [NavigationComponent]
})
export class MainModule {
  constructor() {
    console.log('main module init')
  } }
