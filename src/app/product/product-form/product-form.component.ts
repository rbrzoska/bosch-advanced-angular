import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { Product } from '../../models/product';

@Component({
  selector: 'bsh-product-form',
  templateUrl: './product-form.component.html',
  styleUrls: ['./product-form.component.scss']
})
export class ProductFormComponent implements OnInit {

  @Input() product: Product;
  @Output() submitProduct = new EventEmitter<Product>();

  productForm: FormGroup;
  constructor() {

    this.productForm = new FormGroup({
      stan: new FormControl('tak'),
      title: new FormControl('', Validators.required),
      description: new FormControl(''
        , [Validators.required, Validators.maxLength(1000)]),
      price: new FormControl(0, {
        updateOn: 'change', validators: this.priceRangeValidator
      })
    });
    this.productForm.valueChanges.subscribe(x => console.log(x));

    this.productForm.controls['price'].statusChanges.subscribe(status => {
      if(status === 'INVALID') {
        this.productForm.controls['description'].disable();
      } else {
        this.productForm.controls['description'].enable();
      }
    })
  }

  ngOnInit() {
    if(this.product) {
      let {id, ...productWithoutId} = this.product;
      this.productForm.setValue(
        {
          title: this.product.title,
          description: this.product.description,
          price: this.product.price
        }
      )
    }
  }

  priceRangeValidator(control: AbstractControl): ValidationErrors | null {
    return control.value > 100 ? {
      kurwaJakDrogo: true
    } : null;
  }

  saveProduct() {
    if(this.productForm.valid) {
      this.submitProduct.emit(this.productForm.value)
    }
  }

}
