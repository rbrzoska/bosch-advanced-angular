import { ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { Product } from '../../models/product';
import { TestService } from '../../core/test.service';

@Component({
  selector: 'bsh-product-details',
  templateUrl: './product-details.component.html',
  styleUrls: ['./product-details.component.scss']
})
export class ProductDetailsComponent implements OnInit {

  showDescription = false;
  @Input() product: Product;
  @Output() sprzedaj = new EventEmitter<string>();
  constructor(private testService: TestService) {
    console.log(testService.getString())
  }


  ngOnInit() {
    console.log('init details')
  }
  handleSprzedaj() {
    this.sprzedaj.emit(this.product.title.toUpperCase())
  }
  toggleDescription() {
    this.showDescription = !this.showDescription;
  }

}
