import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { ProductsService } from '../products.service';
import { Product } from '../../models/product';
import { switchMap } from 'rxjs/operators';
import { Observable } from 'rxjs';
import { ProductFormComponent } from '../product-form/product-form.component';
import { NotificationsService } from '../../shared/notifications/notifications.service';

@Component({
  selector: 'bsh-product-edit',
  templateUrl: './product-edit.component.html',
  styleUrls: ['./product-edit.component.scss']
})
export class ProductEditComponent implements OnInit {

  @ViewChild(ProductFormComponent) form: ProductFormComponent;
  product$: Observable<Product>;
  constructor(private activatedRoute: ActivatedRoute,
              private productService: ProductsService,
              private notifications: NotificationsService) {
    this.product$ = activatedRoute.params
      .pipe(
        switchMap(params => productService
          .getProductById(params['id']))
      )
  }

  ngOnInit() {
  }

  save(data) {
    console.log(data)
    this.form.productForm.reset();
    this.notifications.pushNotification('Updated Successfully!')
  }

}
