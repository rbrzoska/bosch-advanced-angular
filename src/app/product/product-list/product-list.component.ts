import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { Product } from '../../models/product';
import { ProductsService } from '../products.service';
import { BehaviorSubject, Subject, Subscription } from 'rxjs';
import { debounceTime, filter, share, tap } from 'rxjs/operators';

@Component({
  selector: 'bsh-product-list',
  templateUrl: './product-list.component.html',
  styleUrls: ['./product-list.component.scss']
})
export class ProductListComponent implements OnInit, OnDestroy {

  numbers = [1,2,3,4,5];
  search$ = new BehaviorSubject<string>('');
  productsSubscription: Subscription;
  products: Product[] = [];
  constructor(public productsService: ProductsService,
              public cdr: ChangeDetectorRef) {
    this.search$.pipe(share())
    this.productsSubscription = this.productsService.getProductsObservable().subscribe(
      products => {
        this.products = products;
      }
    );
    this.search$
      .pipe(
        tap(console.log),
        debounceTime(300)
      )
      .subscribe(q => this.productsService.refreshProducts(q))
    ;
  }

  ngOnInit() {
    console.log('init list')
  }

  onSprzedaj(product: Product) {
    alert(product);
  }

  ngOnDestroy(): void {
    this.productsSubscription.unsubscribe();
  }
}
