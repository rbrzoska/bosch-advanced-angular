import { Injectable } from '@angular/core';
import { Product } from '../models/product';
import { HttpClient } from '@angular/common/http';
import { from, Observable, of, Subject } from 'rxjs';
import { fromArray } from 'rxjs/internal/observable/fromArray';
import { tap } from 'rxjs/operators';
import { NotificationsService } from '../shared/notifications/notifications.service';

const API_URL = 'http://localhost:3000/products';

@Injectable()
export class ProductsService {
  private products$ = new Subject<Product[]>();
  products: Product[] = [];
  constructor(private http: HttpClient, private notifications: NotificationsService) { }

  refreshProducts(searchQuery = '') {
    return this.http.get<Product[]>(API_URL,
      {
        params: {
          'title_like': searchQuery
        }
      }
      ).subscribe(
      products => {
        this.products = products;
        this.products$.next(products)
      }
    );
  }

  getProductsObservable(): Observable<Product[]> {
    return this.products$.asObservable().pipe(
      tap(() => this.notifications.pushNotification('products loaded!'))
    )
  }
  getProductById(id: string): Observable<Product> {
    return this.http.get<Product>(API_URL + '/' + id)
  }

  mockHttp(): Observable<number> {
    // return new Observable(observer => {
    //   observer.next(1);
    //   observer.next(2);
    //   observer.next(3);
    //   observer.next(4);
    //   observer.complete();
    // });
    of(1, 2, 3, 4);
    return fromArray([1,2,3,4])
  }
}
