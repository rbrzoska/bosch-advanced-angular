import { Component, OnInit, ViewChild } from '@angular/core';
import { ProductFormComponent } from '../product-form/product-form.component';
import { NotificationsService } from '../../shared/notifications/notifications.service';

@Component({
  selector: 'bsh-product-add',
  templateUrl: './product-add.component.html',
  styleUrls: ['./product-add.component.scss']
})
export class ProductAddComponent implements OnInit {

  @ViewChild(ProductFormComponent) form: ProductFormComponent;
  constructor(private notifications: NotificationsService) { }

  ngOnInit() {
  }

  save(data) {
    console.log(data)
    this.form.productForm.reset();
    this.notifications.pushNotification('Added Successfully!')
  }
}
