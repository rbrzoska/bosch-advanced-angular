import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './main/home/home.component';
import { KontaktComponent } from './main/kontakt/kontakt.component';
import { PageNotFoundComponent } from './main/page-not-found/page-not-found.component';
import { ProductListComponent } from './product/product-list/product-list.component';
import { AuthGuard } from './core/auth.guard';
import { ProductAddComponent } from './product/product-add/product-add.component';
import { ProductEditComponent } from './product/product-edit/product-edit.component';
import { TwitterComponent } from './twitters/twitter/twitter.component';
import { ParentTestComponent } from './main/parent-test/parent-test.component';
import { WizardDemoComponent } from './wizard/wizard-demo/wizard-demo.component';

const routes: Routes = [
  { path: '', component: HomeComponent, pathMatch: 'full'},
  { path: 'kontakt', component: KontaktComponent},
  { path: 'products', loadChildren: './product/product.module#ProductModule', canLoad: [AuthGuard]},
  { path: 'twitter', component: TwitterComponent},
  { path: 'test', component: ParentTestComponent},
  { path: 'wizard', component: WizardDemoComponent},
  { path: '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
