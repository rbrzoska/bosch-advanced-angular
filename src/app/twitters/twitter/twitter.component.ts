import { AfterViewInit, Component, ElementRef, OnInit, QueryList, ViewChild, ViewChildren } from '@angular/core';
import { TwitterPostComponent } from '../twitter-post/twitter-post.component';
import { fromEvent, Observable } from 'rxjs';

@Component({
  selector: 'bsh-twitter',
  templateUrl: './twitter.component.html',
  styleUrls: ['./twitter.component.scss']
})
export class TwitterComponent implements OnInit, AfterViewInit {

  @ViewChild('searchInput') myInput: ElementRef;
  @ViewChildren(TwitterPostComponent, {
    read: TwitterPostComponent
  }) twitterPosts: QueryList<TwitterPostComponent>;
  constructor() {
  }

  ngOnInit() {

  }

  ngAfterViewInit(): void {
    fromEvent(this.myInput.nativeElement, 'keyup')
        .subscribe(ev => console.log(ev['target'].value));
    console.log(this.myInput);
    console.log(this.twitterPosts.length);
    setTimeout(() => this.twitterPosts.first.toggle());
  }

}
