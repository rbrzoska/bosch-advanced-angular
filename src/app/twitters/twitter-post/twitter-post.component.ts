import { AfterContentInit, Component, ContentChild, OnInit } from '@angular/core';

@Component({
  selector: 'bsh-twitter-post',
  templateUrl: './twitter-post.component.html',
  styleUrls: ['./twitter-post.component.scss']
})
export class TwitterPostComponent implements OnInit, AfterContentInit {

  @ContentChild('post') content;
  toggle() {
    this.isCollapsed = !this.isCollapsed;
  }

  isCollapsed = true;
  ngOnInit() {
  }

  ngAfterContentInit(): void {
    console.log(this.content)
  }

}
