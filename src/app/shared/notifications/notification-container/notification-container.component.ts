import {
  Component,
  ComponentFactory,
  ComponentFactoryResolver,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import { NotificationComponent } from '../notification/notification.component';
import { NotificationsService } from '../notifications.service';

@Component({
  selector: 'bsh-notification-container',
  templateUrl: './notification-container.component.html',
  styleUrls: ['./notification-container.component.scss']
})
export class NotificationContainerComponent implements OnInit {

  private componentFactory: ComponentFactory<NotificationComponent>;
  @ViewChild('notificationContainer', {read: ViewContainerRef}) container: ViewContainerRef;
  constructor(private cfr: ComponentFactoryResolver, private notificationsService: NotificationsService) {
    this.componentFactory = this.cfr.resolveComponentFactory(NotificationComponent);
    this.notificationsService.messages$.subscribe(
      msg => this.notify(msg)
    );
  }

  ngOnInit() {
  }

  notify(msg: string) {
    const component = this.container.createComponent(this.componentFactory);
    component.instance.message = msg;
    setTimeout(() => {
      component.destroy();
    }, 3000)
  }

}
