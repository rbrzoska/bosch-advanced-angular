import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class NotificationsService {

  private message = new Subject<string>();
  constructor() { }

  get messages$(): Observable<string> {
    return this.message.asObservable();
  }
  pushNotification(msg: string) {
    this.message.next(msg)
  }
}
