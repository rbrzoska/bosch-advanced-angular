import { Directive, Input, OnInit, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({
  selector: '[bshRepeat]'
})
export class RepeatDirective implements OnInit {

  @Input('bshRepeatOf') bshRepeat: number;
  constructor(private viewRef: ViewContainerRef,
              private template: TemplateRef<any>) { }

              ngOnInit(): void {
    for( let i = 1; i <=this.bshRepeat; i++) {

      this.viewRef.createEmbeddedView(this.template, {
        $implicit: i,
        double: i*2
      });
    }
              }
}
