import { Component, forwardRef, OnInit } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
  selector: 'bsh-tristate-checkbox',
  templateUrl: './tristate-checkbox.component.html',
  styleUrls: ['./tristate-checkbox.component.scss'],
  providers: [{provide: NG_VALUE_ACCESSOR, useExisting: TristateCheckboxComponent, multi: true}]
})
export class TristateCheckboxComponent implements ControlValueAccessor  {
  onChange = (_: any) => {};
  isDisabled = false;
  _controlState: string;

  get controlState() {
    return this._controlState;
  }

  set controlState(val) {
    this._controlState = val;
    this.onChange(val);
  }
  constructor() { }

  registerOnChange(fn: any): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
  }

  setDisabledState(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }

  writeValue(val: string): void {
    this.controlState = val;
  }

}
