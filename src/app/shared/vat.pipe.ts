import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'vat'
})
export class VatPipe implements PipeTransform {

  transform(value: any, percent: string = ''): any {
    return value  + `+ ${percent} VAT`;
  }

}
