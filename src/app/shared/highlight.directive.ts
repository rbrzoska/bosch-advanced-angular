import { Directive, ElementRef, HostListener, Input, Renderer2 } from '@angular/core';

@Directive({
  selector: '[bshHighlight]'
})
export class HighlightDirective {

  @Input('bshHighlight') className;

  @HostListener('mouseover')
  onMouseOver() {
    this.renderer.addClass(this.el.nativeElement, this.className);
  }
  @HostListener('mouseout')
  onMouseOut() {
    this.renderer.removeClass(this.el.nativeElement, this.className);
  }
  constructor(private el: ElementRef, private renderer: Renderer2) { }

}
