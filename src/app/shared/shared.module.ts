import { ModuleWithProviders, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationComponent } from './notifications/notification/notification.component';
import { NotificationContainerComponent } from './notifications/notification-container/notification-container.component';
import { HighlightDirective } from './highlight.directive';
import { RepeatDirective } from './repeat.directive';
import { TristateCheckboxComponent } from './tristate-checkbox/tristate-checkbox.component';
import { VatPipe } from './vat.pipe';
import { NotificationsService } from './notifications/notifications.service';

@NgModule({
  declarations: [NotificationComponent,
  NotificationContainerComponent,
  HighlightDirective,
  RepeatDirective,
  TristateCheckboxComponent,
  VatPipe],
  imports: [
    CommonModule
  ],
  exports: [
    NotificationContainerComponent,
    HighlightDirective,
    RepeatDirective,
    TristateCheckboxComponent,
    VatPipe
  ],
  entryComponents: [NotificationComponent]
})
export class SharedModule {
  constructor() {
    console.log('app module init')
  }

  static forRoot(): ModuleWithProviders {
    return {
      ngModule: SharedModule,
      providers: [NotificationsService]
    }
  }
}
