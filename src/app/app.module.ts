import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { TwitterComponent } from './twitters/twitter/twitter.component';
import { TwitterPostComponent } from './twitters/twitter-post/twitter-post.component';
import { ImagePostComponent } from './twitters/image-post/image-post.component';
import { TextPostComponent } from './twitters/text-post/text-post.component';
import { TOKEN } from './tokens';
import { CoreModule } from './core/core.module';
import { MainModule } from './main/main.module';
import { SharedModule } from './shared/shared.module';
import { WizardComponent } from './wizard/wizard/wizard.component';
import { WizardDemoComponent } from './wizard/wizard-demo/wizard-demo.component';
import { Step1Component } from './wizard/step1/step1.component';
import { Step2Component } from './wizard/step2/step2.component';
import { Step3Component } from './wizard/step3/step3.component';
import { BaseStepComponent } from './wizard/base-step/base-step.component';
import { MylibModule } from 'mylib';

export function myInitializer() {
  return function () {
    console.log('initialize app');
  }
}

// ProductsModule
// TwittersModule
// MainModule


@NgModule({
  declarations: [
    AppComponent,
    TwitterComponent,
    TwitterPostComponent,
    ImagePostComponent,
    TextPostComponent,
    WizardComponent,
    WizardDemoComponent,
    Step1Component,
    Step2Component,
    Step3Component
  ],
  imports: [
    MylibModule,
    CoreModule,
    MainModule,
    SharedModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [
    {provide: TOKEN, useValue: 'value1', multi: true},
    {provide: TOKEN, useValue: 'value2', multi: true},
    {provide: TOKEN, useValue: 'value3', multi: true}],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor() {
    console.log('app module init')
  }
}
