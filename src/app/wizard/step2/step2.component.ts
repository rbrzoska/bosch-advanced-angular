import { Component, OnInit } from '@angular/core';
import { BaseStepComponent } from '../base-step/base-step.component';

@Component({
  selector: 'bsh-step2',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.scss'],
  providers: [{provide: BaseStepComponent, useExisting: Step2Component}]
})
export class Step2Component extends BaseStepComponent{

  isActive = false;
  resultFromStep = {
    data2: 2
  };

  next() {
  }

}
