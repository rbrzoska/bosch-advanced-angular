import { Component, OnInit } from '@angular/core';
import { BaseStepComponent } from '../base-step/base-step.component';

@Component({
  selector: 'bsh-step1',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.scss'],
  providers: [{provide: BaseStepComponent, useExisting: Step1Component}]
})
export class Step1Component extends BaseStepComponent{

  isActive = false;
  resultFromStep = {
    data1: 1
  };

  next() {
  }

}
