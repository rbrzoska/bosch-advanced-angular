import { AfterContentInit, Component, ContentChildren, OnInit, QueryList } from '@angular/core';
import { BaseStepComponent } from '../base-step/base-step.component';

@Component({
  selector: 'bsh-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.scss']
})
export class WizardComponent implements OnInit, AfterContentInit {

  activeId = 0;
  @ContentChildren(BaseStepComponent) steps: QueryList<BaseStepComponent>
  constructor() { }


  ngOnInit() {
  }

  ngAfterContentInit() {
    this.steps.first.isActive = true;
  }
  nextStep() {
    this.steps.toArray()[this.activeId].isActive = false;
    this.activeId++;
    this.steps.toArray()[this.activeId].isActive = true;
  }

}
