import { Component, OnInit } from '@angular/core';


export abstract class BaseStepComponent {
  isActive: boolean;
  resultFromStep: any;
  next(){}
}
