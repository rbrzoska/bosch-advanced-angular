import { Component, OnInit } from '@angular/core';
import { BaseStepComponent } from '../base-step/base-step.component';

@Component({
  selector: 'bsh-step3',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.scss'],
  providers: [{provide: BaseStepComponent, useExisting: Step3Component}]
})
export class Step3Component extends BaseStepComponent{

  isActive = false;
  resultFromStep = {
    data3: 3
  };

  next() {
  }
}
