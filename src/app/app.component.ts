import { Component, Inject } from '@angular/core';
import { TOKEN } from './tokens';

@Component({
  selector: 'bsh-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'bosch';
  constructor(@Inject(TOKEN) private token: string[]){
    console.log(token)
  }
}
